/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wutinan.circle1;

/**
 *
 * @author INGK
 */
public class TestCircle {
    public static void main(String[] args) {
        Circle1 Circle2 = new Circle1(3);
        System.out.println("Area of Circle(r = "+ Circle2.getR() +") is "+ Circle2.calArea());
        Circle2.setR(2);
        System.out.println("Area of Circle(r = "+ Circle2.getR() +") is "+ Circle2.calArea());
        Circle2.setR(0);
        System.out.println("Area of Circle(r = "+ Circle2.getR() +") is "+ Circle2.calArea());
    }
}
